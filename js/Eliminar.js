// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, updateDoc, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBywpzjkO5Ue4dEExIBoIEF4PJuwmKHpEE",
  authDomain: "tops-39e6d.firebaseapp.com",
  projectId: "tops-39e6d",
  storageBucket: "tops-39e6d.appspot.com",
  messagingSenderId: "1056530990399",
  appId: "1:1056530990399:web:e58f750a6b1301525e7f8a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

document.addEventListener('DOMContentLoaded', function () {
  // Obtiene los datos del producto almacenados en localStorage
  const productoDataString = localStorage.getItem('datoProducto');
  console.log('Datos recuperados de localStorage:', productoDataString);

  // Convierte la cadena JSON a un objeto JavaScript
  const productoData = JSON.parse(productoDataString);

  if (productoData && productoData.nombre_producto) {
    document.getElementById('nombre').value = productoData.nombre_producto;
    console.log('Nombre del producto:', productoData.nombre_producto);
    document.getElementById('preci').value = productoData.precio;
    console.log(productoData.precio);
    document.getElementById('desc').value = productoData.descripcion;
    document.getElementById('descu').value = productoData.descuento;
    document.getElementById('img').value = productoData.imagen;
    switch (productoData.marca) {
      case 'Avene': {
        // Obtén el elemento select por su id
        let marcaSelect = document.getElementById('cat');

        const optAvene = document.createElement('option');
        const optNeutrogena = document.createElement('option');
        const optCerave = document.createElement('option');
        const optPonds = document.createElement('option');
        const optOrdinary = document.createElement('option');

        // Asigna el texto y el valor de la opción
        optAvene.text = 'Avene';
        optAvene.value = 'Avene';
        optNeutrogena.text = 'Neutrogena';
        optNeutrogena.value = 'Neutrogena';
        optCerave.text = 'Cerve';
        optCerave.value = 'Cerve';
        optPonds.text = 'Ponds';
        optPonds.value = 'Ponds';
        optOrdinary.text = 'Ordinary';
        optOrdinary.value = 'Ordinary';

        // Agrega la opción al select
        marcaSelect.appendChild(optAvene);
        marcaSelect.appendChild(optNeutrogena);
        marcaSelect.appendChild(optCerave);
        marcaSelect.appendChild(optPonds);
        marcaSelect.appendChild(optOrdinary);

        break;
      }

      case 'Neutrogena': {
        // Obtén el elemento select por su id
        let marcaSelect = document.getElementById('cat');

        const optAvene = document.createElement('option');
        const optNeutrogena = document.createElement('option');
        const optCerave = document.createElement('option');
        const optPonds = document.createElement('option');
        const optOrdinary = document.createElement('option');

        // Asigna el texto y el valor de la opción
        optAvene.text = 'Avene';
        optAvene.value = 'Avene';
        optNeutrogena.text = 'Neutrogena';
        optNeutrogena.value = 'Neutrogena';
        optCerave.text = 'Cerve';
        optCerave.value = 'Cerve';
        optPonds.text = 'Ponds';
        optPonds.value = 'Ponds';
        optOrdinary.text = 'Ordinary';
        optOrdinary.value = 'Ordinary';

        // Agrega la opción al select
        marcaSelect.appendChild(optNeutrogena);
        marcaSelect.appendChild(optAvene);
        marcaSelect.appendChild(optCerave);
        marcaSelect.appendChild(optPonds);
        marcaSelect.appendChild(optOrdinary);

        break;
      }

      case 'Cerve': {
        // Obtén el elemento select por su id
        let marcaSelect = document.getElementById('cat');

        const optAvene = document.createElement('option');
        const optNeutrogena = document.createElement('option');
        const optCerave = document.createElement('option');
        const optPonds = document.createElement('option');
        const optOrdinary = document.createElement('option');

        // Asigna el texto y el valor de la opción
        optAvene.text = 'Avene';
        optAvene.value = 'Avene';
        optNeutrogena.text = 'Neutrogena';
        optNeutrogena.value = 'Neutrogena';
        optCerave.text = 'Cerve';
        optCerave.value = 'Cerve';
        optPonds.text = 'Ponds';
        optPonds.value = 'Ponds';
        optOrdinary.text = 'Ordinary';
        optOrdinary.value = 'Ordinary';

        // Agrega la opción al select
        marcaSelect.appendChild(optCerave);
        marcaSelect.appendChild(optNeutrogena);
        marcaSelect.appendChild(optAvene);
        marcaSelect.appendChild(optPonds);
        marcaSelect.appendChild(optOrdinary);

        break;
      }

      case 'Ponds': {
        // Obtén el elemento select por su id
        let marcaSelect = document.getElementById('cat');

        const optAvene = document.createElement('option');
        const optNeutrogena = document.createElement('option');
        const optCerave = document.createElement('option');
        const optPonds = document.createElement('option');
        const optOrdinary = document.createElement('option');

        // Asigna el texto y el valor de la opción
        optAvene.text = 'Avene';
        optAvene.value = 'Avene';
        optNeutrogena.text = 'Neutrogena';
        optNeutrogena.value = 'Neutrogena';
        optCerave.text = 'Cerve';
        optCerave.value = 'Cerve';
        optPonds.text = 'Ponds';
        optPonds.value = 'Ponds';
        optOrdinary.text = 'Ordinary';
        optOrdinary.value = 'Ordinary';

        // Agrega la opción al select
        marcaSelect.appendChild(optPonds);
        marcaSelect.appendChild(optCerave);
        marcaSelect.appendChild(optNeutrogena);
        marcaSelect.appendChild(optAvene);
        marcaSelect.appendChild(optOrdinary);

        break;
      }

      case 'Ordinary': {
        // Obtén el elemento select por su id
        let marcaSelect = document.getElementById('cat');

        const optAvene = document.createElement('option');
        const optNeutrogena = document.createElement('option');
        const optCerave = document.createElement('option');
        const optPonds = document.createElement('option');
        const optOrdinary = document.createElement('option');

        // Asigna el texto y el valor de la opción
        optAvene.text = 'Avene';
        optAvene.value = 'Avene';
        optNeutrogena.text = 'Neutrogena';
        optNeutrogena.value = 'Neutrogena';
        optCerave.text = 'Cerve';
        optCerave.value = 'Cerve';
        optPonds.text = 'Ponds';
        optPonds.value = 'Ponds';
        optOrdinary.text = 'Ordinary';
        optOrdinary.value = 'Ordinary';

        // Agrega la opción al select
        marcaSelect.appendChild(optOrdinary);
        marcaSelect.appendChild(optPonds);
        marcaSelect.appendChild(optCerave);
        marcaSelect.appendChild(optNeutrogena);
        marcaSelect.appendChild(optAvene);

        break;
      }
    }

  } else {
    console.error("El objeto productoData o su propiedad nombre_producto es nulo o indefinido.");
  }
});

//Funcion para editar el producto
const btnUpdate = document.getElementById('btnUpdate');

document.addEventListener('DOMContentLoaded', function () {
  btnUpdate.addEventListener('click', async function () {
    //Obtener los nuevos datos para actualizar
    const newName = document.getElementById('nombre').value;
    const newDescription = document.getElementById('desc').value;
    const newPrice = document.getElementById('preci').value;
    const newDescu = document.getElementById('descu').value;
    const newImg = document.getElementById('img').value;
    const newMarca = document.getElementById('cat').value;

    //Obtener los nombres antiguos almacenados en localStorage
    const productoDataString = localStorage.getItem('datoProducto');
    const productoData = JSON.parse(productoDataString);
    const nombreAntiguo = productoData.nombre_producto;
    console.log(productoData.nombre_producto);

    // Verificar si nombreAntiguo tiene un valor
    if (nombreAntiguo) {
      // Encuentra el documento en la base de datos con el nombre del producto
      const productosCollection = collection(db, 'productos');
      const q = query(productosCollection, where('nombre_producto', '==', nombreAntiguo));
      console.log(nombreAntiguo);
      console.log(q);

      const productosSnapshot = await getDocs(q);
      if (!productosSnapshot.empty) {
        const productoDoc = productosSnapshot.docs[0];

        // Actualiza el documento en la base de datos
        await updateDoc(productoDoc.ref, {
          nombre_producto: newName,
          descripcion: newDescription,
          precio: newPrice,
          descuento: newDescu,
          imagen: newImg,
          marca: newMarca
          // Agrega otros campos según sea necesario
        });

        // Puedes redirigir o mostrar un mensaje de éxito aquí
        alert("Producto actualizado con éxito");
        window.location.href = '../html/inventario.html';
      } else {
        alert("Error al actualizar el producto");
      }
    } else {
      console.error("El valor de nombreAntiguo es undefined.");
    }
  });
});

