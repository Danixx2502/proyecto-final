// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, query, where, deleteDoc } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBywpzjkO5Ue4dEExIBoIEF4PJuwmKHpEE",
  authDomain: "tops-39e6d.firebaseapp.com",
  projectId: "tops-39e6d",
  storageBucket: "tops-39e6d.appspot.com",
  messagingSenderId: "1056530990399",
  appId: "1:1056530990399:web:e58f750a6b1301525e7f8a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

//variables
const agregar = document.getElementById('agregar');

//funcion del boton agregar
agregar.addEventListener('click', async function () {
  window.location = "../html/db.html";
});
/* Se agarra el boton y te manda a la pagina para agregar a la bd*/

//Funcion apra el boton que agregamos dinamicamente editar que te redirige a la pagina
window.edit = function (nombre, descripcion, precio, descuento, marca, imagen) {
  // Crea un objeto con los datos del producto
  const producto = {
    nombre_producto: nombre,
    descripcion: descripcion,
    precio: precio,
    imagen: imagen,
    marca: marca,
    descuento: descuento
  };
  // Almacena los datos del producto en localStorage
  localStorage.setItem('datoProducto', JSON.stringify(producto));
  console.log("Datos almacenados en localStorage:", producto);

  // Redirecciona al usuario a la página de edición con los datos
  window.location.href = "../html/Eliminar.html";
}





//funcion para cargar los datos
const datos = async (marca, tabla) => {
  try {
    console.log("Iniciar funcion");
    const collecProduc = collection(db, 'productos');
    const produc = query(collecProduc, where('marca', "==", marca));
    const obtener = await getDocs(produc);
    const section = document.getElementById(tabla); //obtencion de datos del inventario
    let content = '';
    obtener.docs.forEach((doc) => {
      const datoProducto = doc.data();
      //crear la muestra para mostrar los datos
      //console.log("enviar datos");
      content += `
          <tr>
              <th>Imagen</th>
              <td> <img src=" ${datoProducto.imagen}" alt=""> </td>
              
          </tr>
          <tr>
             <th>Marca</th>
          <td> <p> ${datoProducto.marca} </p></td>
          </tr>
          <tr>
              <th>Nombre del producto</th>
              <td> <p> ${datoProducto.nombre_producto}</p> </td>
          </tr>
           <tr>
              <th>Descuento</th>
          <td> <p> ${datoProducto.descuento}</p></td>
          </tr>
      
          <tr>
             <th>Precio</th>
          <td> <p> ${datoProducto.precio}</p></td>
          </tr>
      
          <tr>
              <th>Descripción</th>
          <td><p> ${datoProducto.descripcion}</p></td>
          </tr>
          <tr>
    <td>
        <div class="button-container">
        <button onclick="edit('${datoProducto.nombre_producto}','${datoProducto.descripcion}','${datoProducto.precio}','${datoProducto.descuento}','${datoProducto.marca}','${datoProducto.imagen}')" class="button">Editar</button>
        <button onclick="deleteProduct('${datoProducto.nombre_producto}')" class="button">Borrar</button>
        </div>
    </td>
</tr>
          `;
    });
    section.innerHTML = content;
  } catch (error) {
    console.error(error);
  }
}


//Funcion para eliminar un producto de la base de datos
window.deleteProduct = function (nombre) {
  // Obtén la referencia a la colección "productos" en Firestore
  const productosCollection = collection(getFirestore(), 'productos');
  // Consulta el producto por nombre
  const q = query(productosCollection, where('nombre_producto', '==', nombre));

  // Ejecuta la consulta y maneja la promesa resultante
  getDocs(q)
    .then(querySnapshot => {
      // Verifica si existe el producto en la base de datos
      if (!querySnapshot.empty) {
        // Obtiene el documento del primer resultado
        const productDoc = querySnapshot.docs[0];

        // Elimina el documento de la base de datos
        deleteDoc(productDoc.ref)
          .then(() => {
            alert('Producto eliminado correctamente');
            window.location.href = '../html/inventario.html';
          })
          .catch(error => {
            alert('Error al eliminar el producto:', error);
          });
      } else {
        alert(`No se encontró el producto "${nombre}" en la base de datos.`);
      }
    })
    .catch(error => {
      alert('Error en la función deleteProduct:', error);

    });
};

datos('Avene', 'tabla');
datos('Ordinary', 'tabla2');
datos('Cerave', 'tabla3');
datos('Neutrogena', 'tabla4');

