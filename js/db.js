// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, addDoc } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBywpzjkO5Ue4dEExIBoIEF4PJuwmKHpEE",
  authDomain: "tops-39e6d.firebaseapp.com",
  projectId: "tops-39e6d",
  storageBucket: "tops-39e6d.appspot.com",
  messagingSenderId: "1056530990399",
  appId: "1:1056530990399:web:e58f750a6b1301525e7f8a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

//obtiene los datos
const db = getFirestore(app);

// La función saveProductos se define como una función asíncrona que recibe un objeto 'producto' como argumento
const saveProductos = async (producto) => {
    try {
        // Utiliza la función addDoc de Firebase para agregar un nuevo documento en la colección 'productos'
        // La función devuelve una referencia al documento recién creado
        const docRef = await addDoc(collection(db, "productos"), producto);

        // Llama a la función MSJOk para mostrar un mensaje de éxito
    } catch (error) {
        // Si ocurre algún error durante la operación, llama a la función MSJERR para mostrar un mensaje de error
    }
}

const btnaceptar = document.getElementById('aceptar');

btnaceptar.onclick = ()=>{
    let marca = document.getElementById('marca').value;
    let nombre_producto = document.getElementById('nombre_producto').value;
    let imagen = document.getElementById('imagen').value;
    let descuento = document.getElementById('descuento').value;
    let precio = document.getElementById('precio').value;
    let descripcion = document.getElementById('descripcion').value;

    if(imagen === "" || descuento === "" || nombre_producto === "" || precio === "" || descripcion === ""){
        alert("Relle todos los campos");
        return false;
    }
    else{
        const get_datos = { //este es mi objeto que guarda los 6 atributos de abajo
            marca: marca,
            imagen:imagen,
            nombre_producto:nombre_producto,
            descuento: descuento,
            precio: precio,
            descripcion: descripcion
        }; 
        document.getElementById('marca').value = "";
        document.getElementById('nombre_producto').value  = "";
        document.getElementById('imagen').value  = "";
        document.getElementById('descuento').value  = "";
        document.getElementById('precio').value  = "";
        document.getElementById('descripcion').value = "";
        

        saveProductos(get_datos);
        
    }

};
