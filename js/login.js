// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBywpzjkO5Ue4dEExIBoIEF4PJuwmKHpEE",
    authDomain: "tops-39e6d.firebaseapp.com",
    projectId: "tops-39e6d",
    storageBucket: "tops-39e6d.appspot.com",
    messagingSenderId: "1056530990399",
    appId: "1:1056530990399:web:e58f750a6b1301525e7f8a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('form').addEventListener('submit', async function (e) {
        e.preventDefault();

        const username = document.getElementById('username').value;
        const passwords = document.getElementById('password').value;

        try {
            const userscollection = collection(db, 'usurios');
            const usersQuery = query(userscollection, where('usuario', '==', username));
            const userSnapshot = await getDocs(usersQuery);

            if (userSnapshot.size > 0) {
                const userData = userSnapshot.docs[0].data();

                if (userData.password === passwords) {

                    alert("Bienvenido administrador");

                    window.location.href = '../html/inventario.html';

                } else {
                    alert("Algunos de los campos estan erroneos")
                }
            } else {
                alert("No se encontraron usuarios con ese nombre o contraseña")
            }
        } catch (error) {
            console.log("Ocurrio un error");
        }

    });

});

