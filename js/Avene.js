// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBywpzjkO5Ue4dEExIBoIEF4PJuwmKHpEE",
    authDomain: "tops-39e6d.firebaseapp.com",
    projectId: "tops-39e6d",
    storageBucket: "tops-39e6d.appspot.com",
    messagingSenderId: "1056530990399",
    appId: "1:1056530990399:web:e58f750a6b1301525e7f8a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);


//

document.addEventListener('DOMContentLoaded', () => {
    const datos = async (marca, tabla) => {
      try {
        console.log("Iniciar función");
        const collecProduc = collection(db, 'productos');
        const produc = query(collecProduc, where('marca', '==', marca));
        const obtener = await getDocs(produc);
        const section = document.getElementById(tabla);
        let content = '';
  
        obtener.docs.forEach((doc) => {
          const datoProducto = doc.data();
          content += `
            <div class="input-container">
              <img width="15px" height="15px" class="square-input" src="${datoProducto.imagen}" alt="">
            </div>
            <div class="input-container">
              <div class="input-group">
                <label for="marca">Marca</label>
                <input type="text" value="${datoProducto.marca}" class="marca" readonly>
              </div>
              <div class="input-group">
                <label for="nombre">Nombre</label>
                <input type="text" value="${datoProducto.nombre_producto}" class="nombre" readonly>
              </div>
              <div class="input-group">
                <label for="descuento">Descuento</label>
                <input type="text" value="${datoProducto.descuento}" class="descuento" readonly>
              </div>
              <div class="input-group">
                <label for="precio">Precio</label>
                <input type="text" value="${datoProducto.precio}" class="precio" readonly>
              </div>
              <div class="input-group">
                <label for="descripcion">Descripción</label>
                <input type="text" value="${datoProducto.descripcion}" class="descripcion" readonly>
              </div>
            </div>
            
            
          `;
        });
  
        section.innerHTML = content;
      } catch (error) {
        console.error(error);
      }
    }
  
    // Llama a la función datos
    datos('Avene', 'tabla');
  });